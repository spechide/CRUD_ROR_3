class ApplicationMailer < ActionMailer::Base
  default from: 'shrimadhavuk@gmail.com'
  layout 'mailer'
end
