json.extract! casefile, :id, :uid, :name, :nature, :offence, :year, :status, :created_at, :updated_at
json.url casefile_url(casefile, format: :json)
