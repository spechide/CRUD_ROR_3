require 'test_helper'

class CasefilesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @casefile = casefiles(:one)
  end

  test "should get index" do
    get casefiles_url
    assert_response :success
  end

  test "should get new" do
    get new_casefile_url
    assert_response :success
  end

  test "should create casefile" do
    assert_difference('Casefile.count') do
      post casefiles_url, params: { casefile: { name: @casefile.name, nature: @casefile.nature, offence: @casefile.offence, status: @casefile.status, uid: @casefile.uid, year: @casefile.year } }
    end

    assert_redirected_to casefile_url(Casefile.last)
  end

  test "should show casefile" do
    get casefile_url(@casefile)
    assert_response :success
  end

  test "should get edit" do
    get edit_casefile_url(@casefile)
    assert_response :success
  end

  test "should update casefile" do
    patch casefile_url(@casefile), params: { casefile: { name: @casefile.name, nature: @casefile.nature, offence: @casefile.offence, status: @casefile.status, uid: @casefile.uid, year: @casefile.year } }
    assert_redirected_to casefile_url(@casefile)
  end

  test "should destroy casefile" do
    assert_difference('Casefile.count', -1) do
      delete casefile_url(@casefile)
    end

    assert_redirected_to casefiles_url
  end
end
