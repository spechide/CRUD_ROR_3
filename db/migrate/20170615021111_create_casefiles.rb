class CreateCasefiles < ActiveRecord::Migration[5.1]
  def change
    create_table :casefiles do |t|
      t.string :uid
      t.string :name
      t.string :nature
      t.string :offence
      t.date :year
      t.string :status

      t.timestamps
    end
  end
end
