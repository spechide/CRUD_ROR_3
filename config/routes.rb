Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :casefiles
  get 'hello_world/index'
  root 'hello_world#index'
end
